package org.scrum.psd.battleship.ascii;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;

import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;

@Execution(ExecutionMode.CONCURRENT)
public class MainTest {

    public static final String ANSI_GREEN_TEXT_CODE = "[32m";
    public static final String ANSI_RED_TEXT_CODE = "[31m";

    @Test
    public void testParsePosition() {
        Position actual = Main.parsePosition("A1");
        Position expected = new Position(Letter.A, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testParsePosition2() {
        //given
        Position expected = new Position(Letter.B, 1);
        //when
        Position actual = Main.parsePosition("B1");
        //then
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void given_PositiveMessage_shouldReturnGreenText() {
        //given

        String message = Main.getPositiveMessage("This is positive");

        Assertions.assertTrue(message.contains(ANSI_GREEN_TEXT_CODE));

    }

    @Test
    public void given_NegativeMessage_shouldReturnRedText() {
        //given

        String message = Main.getNegativeMessage("This is negative");

        Assertions.assertTrue(message.contains(ANSI_RED_TEXT_CODE));

    }
}
