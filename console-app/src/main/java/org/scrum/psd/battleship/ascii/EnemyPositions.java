package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class EnemyPositions {

    private static final List<Position> enemyUsedPositions = new ArrayList<>();

    public static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;

        List<Position> allPossiblePositions = IntStream.range(0, lines).boxed().map(i -> Letter.values()[i])
                .flatMap(l -> IntStream.range(0, rows).boxed().map(i -> new Position(l, i + 1)))
                .collect(Collectors.toList());

        List<Position> candidatePositions = allPossiblePositions.stream().filter(p -> !enemyUsedPositions.contains(p))
                .collect(Collectors.toList());

        Random random = new Random();
        Position position = candidatePositions.get(random.nextInt(candidatePositions.size()));
        enemyUsedPositions.add(position);
        return position;
    }

    public static String getUsedPostionsString(){
        return "Enemy Used Positions: " + String.join(", ", enemyUsedPositions.stream().map(Position::toString).collect(Collectors.toList()));
    }
}
