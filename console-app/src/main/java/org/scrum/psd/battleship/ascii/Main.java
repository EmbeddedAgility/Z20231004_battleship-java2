package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.*;
import java.util.stream.Collectors;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.*;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;

    private static int rows = 8;
    private static int lines = 8;

    private static final Telemetry telemetry = new Telemetry();
    private static Scanner scanner;

    private static WinnerState winnerState = WinnerState.No;

    public static void main(String[] args) {
        telemetry.trackEvent("ApplicationStarted", "Technology", "Java");
        System.out.println(colorize("                                     |__", MAGENTA_TEXT()));
        System.out.println(colorize("                                     |\\/", MAGENTA_TEXT()));
        System.out.println(colorize("                                     ---", MAGENTA_TEXT()));
        System.out.println(colorize("                                     / | [", MAGENTA_TEXT()));
        System.out.println(colorize("                              !      | |||", MAGENTA_TEXT()));
        System.out.println(colorize("                            _/|     _/|-++'", MAGENTA_TEXT()));
        System.out.println(colorize("                        +  +--|    |--|--|_ |-", MAGENTA_TEXT()));
        System.out.println(colorize("                     { /|__|  |/\\__|  |--- |||__/", MAGENTA_TEXT()));
        System.out.println(colorize("                    +---------------___[}-_===_.'____                 /\\", MAGENTA_TEXT()));
        System.out.println(colorize("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _", MAGENTA_TEXT()));
        System.out.println(colorize(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7", MAGENTA_TEXT()));
        System.out.println(colorize("|                        Welcome to Battleship                         BB-61/", MAGENTA_TEXT()));
        System.out.println(colorize(" \\_________________________________________________________________________|", MAGENTA_TEXT()));
        System.out.println("");
        scanner = new Scanner(System.in);
        InitializeGame();

        StartGame();
    }

    private static void StartGame() {

        int turnNumber = 1;
        System.out.print("\033[2J\033[;H");
        System.out.println("                  __");
        System.out.println("                 /  \\");
        System.out.println("           .-.  |    |");
        System.out.println("   *    _.-'  \\  \\__/");
        System.out.println("    \\.-'       \\");
        System.out.println("   /          _/");
        System.out.println("  |      _  /\" \"");
        System.out.println("  |     /_\'");
        System.out.println("   \\    \\_/");
        System.out.println("    \" \"\" \"\" \"\" \"");

        do {
            System.out.println("********************* Turn Number: " + turnNumber +" *************************************");
            System.out.println("Player, it's your turn");
            System.out.println("You can choose between coordinates A-H / 1-8. for eg: A1");
            System.out.println("Enter coordinates for your shot :");
            Position position;

            try {
                position = parsePosition(scanner.next());
            } catch (OutOfPositionException ex) {
                System.out.println(colorize("Your shooting position is invalid. Please shoot within A-H/1-8.", YELLOW_TEXT()));
                continue;
            }

            turnNumber++;


            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            if (isHit) {
                beep();

                System.out.println("                \\         .  ./");
                System.out.println("              \\      .:\" \";'.:..\" \"   /");
                System.out.println("                  (M^^.^~~:.'\" \").");
                System.out.println("            -   (/  .    . . \\ \\)  -");
                System.out.println("               ((| :. ~ ^  :. .|))");
                System.out.println("            -   (\\- |  \\ /  |  /)  -");
                System.out.println("                 -\\  \\     /  /-");
                System.out.println("                   \\  \\   /  /");

                if (isAllSunk(enemyFleet)) {
                    System.out.println(colorize("You are the winner!", GREEN_TEXT()));
                    winnerState = WinnerState.Player;
                    break;
                }
            }

            System.out.println(getAttackEnemyMessage(isHit));
            telemetry.trackEvent("Player_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
            printShipsLeftAndShipsAlive(enemyFleet, "Enemy Fleet");

            position = EnemyPositions.getRandomPosition();
            System.out.println(colorize(EnemyPositions.getUsedPostionsString(), RED_BACK()));
            isHit = GameController.checkIsHit(myFleet, position);

            System.out.println("");
            System.out.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(),
                                             getAttackByEnemyMessage(isHit)));
            telemetry.trackEvent("Computer_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
            if (isHit) {
                beep();

                System.out.println("                \\         .  ./");
                System.out.println("              \\      .:\" \";'.:..\" \"   /");
                System.out.println("                  (M^^.^~~:.'\" \").");
                System.out.println("            -   (/  .    . . \\ \\)  -");
                System.out.println("               ((| :. ~ ^  :. .|))");
                System.out.println("            -   (\\- |  \\ /  |  /)  -");
                System.out.println("                 -\\  \\     /  /-");
                System.out.println("                   \\  \\   /  /");

                if (isAllSunk(myFleet)) {
                    System.out.println(colorize("You lost!", RED_TEXT()));
                    winnerState = WinnerState.Enemy;
                    break;
                }
            }
            printShipsLeftAndShipsAlive(myFleet, "Player Fleet");
            System.out.println("**********************************************************");
            System.out.println();
        } while (true);
    }

    private static void printShipsLeftAndShipsAlive(List<Ship> ships, String fleetName) {
        String shipsLeftString = String.join(",", ships.stream().filter(ship -> !ship.isSunk()).map(Ship::getName).collect(Collectors.toList()));
        String shipsSunkString = String.join(",", ships.stream().filter(ship -> ship.isSunk()).map(Ship::getName).collect(Collectors.toList()));
        System.out.println(fleetName + " left : " + shipsLeftString);
        System.out.println(fleetName + " sunk : " + shipsSunkString);
    }

    private static String getAttackByEnemyMessage(boolean isHit) {
        return isHit ? getNegativeMessage("Enemy hit your ship !") : getPositiveMessage("Enemy Miss");
    }

    private static String getAttackEnemyMessage(boolean isHit) {
        return isHit ? getPositiveMessage("You hit an enemy ship ! Nice hit !") : getNegativeMessage("You Miss");
    }

    public static String getPositiveMessage(String message) {
        return colorize(message, GREEN_TEXT());
    }

    public static String getNegativeMessage(String message) {
        return colorize(message, RED_TEXT());
    }

    private static void beep() {
        System.out.print("\007");
    }

    protected static Position parsePosition(String input) {
        try {
            Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
            int number = Integer.parseInt(input.substring(1));
            if (number > lines || number < 1) {
                throw new Exception("Out of line range of " + lines);
            }
            return new Position(letter, number);
        } catch (Exception ex) {
            throw new OutOfPositionException("Position does not exist.", ex);
        }

    }

    private static void InitializeGame() {
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {

        myFleet = GameController.initializeShips();

        System.out.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            System.out.println("");
            System.out.println(String.format("Please enter the positions for the %s (size: %s)", ship.getName(), ship.getSize()));
            for (int i = 1; i <= ship.getSize(); i++) {
                System.out.println(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));

                String positionInput = scanner.next();
                ship.addPosition(positionInput);
                telemetry.trackEvent("Player_PlaceShipPosition", "Position", positionInput, "Ship", ship.getName(), "PositionInShip", Integer.valueOf(i).toString());
            }
        }
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    private static boolean isAllSunk(List<Ship> fleet){
        return fleet.stream().allMatch(Ship::isSunk);
    }

    // TODO winnerState can be used directly
    private WinnerState getWinnerState(){
        return winnerState;
    }

    private static boolean hasWin(WinnerState winnerState) {
        return WinnerState.Player == winnerState;
    }
}