package org.scrum.psd.battleship.ascii;

public enum WinnerState {
    No,
    Player,
    Enemy
}
