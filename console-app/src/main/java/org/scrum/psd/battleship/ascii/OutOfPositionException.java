package org.scrum.psd.battleship.ascii;

public class OutOfPositionException extends RuntimeException {
    public OutOfPositionException(String message) {
        super(message);
    }

    public OutOfPositionException(String message, Throwable cause) {
        super(message, cause);
    }
}
