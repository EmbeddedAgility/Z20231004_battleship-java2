package org.scrum.psd.battleship.controller.dto;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ShipTest {

    @Test
    void hit() {
        Ship ship = new Ship("Test", 3, Arrays.asList(
                new Position(Letter.A, 1),
                new Position(Letter.A, 2),
                new Position(Letter.A, 3)
        ));

        assertTrue(ship.hit(new Position(Letter.A, 1)));
        assertFalse(ship.hit(new Position(Letter.B, 1)));
        assertFalse(ship.hit(new Position(Letter.A, 1)));
        assertTrue(ship.hit(new Position(Letter.A, 2)));
    }

    @Test
    void isSunk() {
        Ship ship = new Ship("Test", 3, Arrays.asList(
                new Position(Letter.A, 1),
                new Position(Letter.A, 2),
                new Position(Letter.A, 3)
        ));

        assertFalse(ship.isSunk());

        ship.hit(new Position(Letter.A, 1));
        ship.hit(new Position(Letter.A, 2));
        ship.hit(new Position(Letter.A, 3));

        assertTrue(ship.isSunk());
    }
}